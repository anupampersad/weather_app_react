import React, { Component } from 'react';
import Container from './container';

import Clear from "./images/Clear.jpg"
import Clouds from "./images/Clouds.jpg"
import Drizzle from "./images/Drizzle.jpg"
import Haze from "./images/Haze.jpg"
import Rain from "./images/Rain.jpg"
import Smoke from "./images/Smoke.jpg"
import Snow from "./images/Snow.jpg"
import sunny from "./images/sunny.jpg"
import Thunderstorm from "./images/Thunderstorm.jpg"
import Mist from "./images/Mist.jpg"


export default class Logic extends Component {

    constructor(props) {

        super(props)

        this.state = {
            data: {},
            isWeatherDetails: false,
            weatherName: Clear,
            cityName: '',
            isError: false,
        }
    }

    location = (event) => {
        this.setState({ cityName: event.target.value })
    }

    fetchData = () => {

        const weatherApi = {
            key: '7b02a47c5c19aef34a703ead188ea611',
            url: 'https://api.openweathermap.org/data/2.5/weather'
        }

        let city = this.state.cityName;

        fetch(`${weatherApi.url}?q=${city}&appid=${weatherApi.key}&units=metric`)
            .then(weather => {
                return weather.json();
            }).then((weather_data) => {

                console.log(weather_data)

                const name = {
                    'Clear': Clear,
                    'Clouds': Clouds,
                    'Drizzle': Drizzle,
                    'Haze': Haze,
                    'Rain': Rain,
                    'Smoke': Smoke,
                    'Snow': Snow,
                    'sunny': sunny,
                    'Thunderstorm': Thunderstorm,
                    'Mist': Mist
                }

                this.setState({
                    data: weather_data,
                    isWeatherDetails: true,
                    weatherName: name[weather_data.weather[0].main],
                    isError:false
                })

            })

            .catch((error) => {
                this.setState({
                    isError:true
                })
            })
    }

    initialFetch = () => {

        const name = {
            'Clear': Clear,
            'Clouds': Clouds,
            'Drizzle': Drizzle,
            'Haze': Haze,
            'Rain': Rain,
            'Smoke': Smoke,
            'Snow': Snow,
            'sunny': sunny,
            'Thunderstorm': Thunderstorm,
            'Mist': Mist
        }

        const weatherApi = {
            key: '7b02a47c5c19aef34a703ead188ea611',
            url: 'https://api.openweathermap.org/data/2.5/weather'
        }

        navigator.geolocation.getCurrentPosition((position) => {

            const lat = position.coords.latitude
            const lon = position.coords.longitude

            fetch(`${weatherApi.url}?lat=${lat}&lon=${lon}&appid=${weatherApi.key}&units=metric`)
                .then(weather => {
                    return weather.json();
                }).then((weather_data) => {

                    this.setState({
                        data: weather_data,
                        isWeatherDetails: true,
                        weatherName: name[weather_data.weather[0].main],
                        isError: false
                    })
                })
                .catch(() => {
                    this.setState({
                        isError: true
                    })
                })

        })
    }

    dateManage = (DateObj) => {
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

        const year = DateObj.getFullYear();
        const month = months[DateObj.getMonth()];
        const date = DateObj.getDate();
        let day = days[DateObj.getDay()];

        return `${date} ${month} (${day}) ${year}`
    }
    

    render() {

        const { data, isWeatherDetails, weatherName, isError } = this.state


        return (
            <div style={{
                backgroundImage: `url(${weatherName})`,
                height: "100vh",
                backgroundRepeat: "no-repeat",
                backgroundPosition: 'bottom center',
                backgroundSize: 'cover'

            }}>
                <Container
                    data={data}
                    fetchData={this.fetchData}
                    isWeatherDetails={isWeatherDetails}
                    isError={isError}
                    location={this.location}
                    weatherName={weatherName}
                    dateManage={this.dateManage}
                    initialFetch={this.initialFetch}
                />
            </div>
        )
    }
}
