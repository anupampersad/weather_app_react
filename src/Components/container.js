import React, { Component } from 'react';
import './container.css';

export default class Container extends Component {

    constructor(props) {
        super(props)
        this.props.initialFetch()
    }

    handleEnter = (event) => {
        if (event.charCode == 13){
            this.props.fetchData()
        }
    }

    render() {

        const { data, fetchData, isWeatherDetails, isError, location, dateManage } = this.props;

        const { main, weather } = data;

        const todaydate = new Date();

        return (

            <div className="main-container">

                <div className="container">

                    <div className="search_input">
                        <input type="text" placeholder="Enter the city name : " id="input_box" className="input_box" onChange={location} onKeyPress={this.handleEnter} />

                        {
                            isError
                                ? <div id="error-message">INVALID INPUT</div>
                                : <></>
                        }

                        <button type="submit" className="submit-button" id="submit-button" onClick={fetchData}>submit</button>
                    </div>

                    {isWeatherDetails

                    ? <div className="weather_details">

                        <div className="city" id="city">{data.name}</div>
                        <div className="date" id="date">{dateManage(todaydate)}</div>
                        <div className="temp" id="temp">{main.temp}&deg;C</div>
                        <div className="min_max" id="min_max">{Math.floor(main.temp_min)}&deg;C (min) / {Math.ceil(main.temp_max)}&deg;C (max)</div>
                        <div className="weather" id="weather">{weather[0].main}</div>
                        <div className="humidity" id="humidity">Humidity : {main.humidity} %</div>
                        <div className="img" id="img">
                            <img src={`http://openweathermap.org/img/wn/${weather[0].icon}@2x.png`}></img>
                        </div>

                    </div>

                    : <></>

                    }


                </div>

            </div>
        )
    }
}